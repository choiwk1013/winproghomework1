// WinProgHomework1.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "WinProgHomework1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//	_tprintf, _tscanf  함수를 편하게 사용하기 위해서
#define printf(x, ...) _tprintf(_T(x), __VA_ARGS__)
#define scanf(x, ...) _tscanf(_T(x), __VA_ARGS__)

void init(void);
void problem01(void);
void problem02(void);
void problem03(void);
void problem04(void);
void problem05(void);
void problem06(void);
void problem07(void);
void problem08(void);
void problem09(void);
void problem10(void);
void problem11(void);


// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: 오류 코드를 필요에 따라 수정합니다.
			_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
			init();

//			problem01();
//			problem02();
//			problem03();
//			problem04();
//			problem05();
//			problem06();
//			problem07();
//			problem08();
//			problem09();
//			problem10();
			problem11();
		}
	}
	else
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: GetModuleHandle 실패\n"));
		nRetCode = 1;
	}

	return nRetCode;
}

void init(void) {
	_tsetlocale(LC_ALL, _T(""));	//	한글 출력이 재대로 되게 하기 위해서
}

void problem01(void) {
	CString str;
	str.LoadString(IDS_MESSAGE);
	printf("%s\n", str);	//	_tprintf(_T("%s\n"), str);    <== 매크로를 사용하지 않을 시 원래 형태
//	_tprintf(_T("%s\n"), str);
}

void problem02(void) {
	CString str1;
	str1.LoadString(IDS_FORMAT);
	
	CString str2;
	str2.Format(str1, _T("최완규"), 23);

	printf("%s\n", str2);
//	_tprintf(_T("%s\n"), str2);
}

void problem03(void) {
	TCHAR buffer[1024];
	scanf("%s", buffer);		//	_tscanf(_T("%s"), buffer);	<== 매크로를 사용하지 않을 시 원래 형태
//	_tscanf(_T("%s"), buffer);

	CString str(buffer);
	printf("%s\n", str.MakeUpper());
//	_tprintf(_T("%s\n"), str.MakeUpper());
}

void problem04(void) {
	TCHAR buffer[1024];
	scanf("%s", buffer);
//	_tscanf(_T("%s"), buffer);

	CString str(buffer);
	str.Replace(_T("."), _T(" "));
	printf("%s\n", str);
//	_tprintf(_T("%s\n"), str);
}

void problem05(void) {
	CRect rect(30, 40, 130, 240);
	printf("Center-(%ld, %ld)\n", rect.CenterPoint().x, rect.CenterPoint().y);
//	_tprintf(_T("Center-(%ld, %ld)\n"), rect.CenterPoint().x, rect.CenterPoint().y);
}

void problem06(void) {
	CRect rect(100, 50, 250, 180);
	printf("폭: %ld, 너비:%ld\n", rect.Size().cx, rect.Size().cy);
//	_tprintf(_T("폭: %ld, 너비:%ld\n"), rect.Size().cx, rect.Size().cy);
}

void problem07(void) {
	CRect rect(10, 20, 30, 40);
	rect.OffsetRect(150, 80);
	printf("top: %ld, left: %ld, right: %ld, bottom: %ld\n", rect.top, rect.left, rect.right, rect.bottom);
//	_tprintf(_T("top: %ld, left: %ld, right: %ld, bottom: %ld\n"), rect.top, rect.left, rect.right, rect.bottom);
}

void problem08(void) {
	CRect rect(80, 45, 190, 135);
	CPoint pt;

	scanf("%d, %d", &pt.x, &pt.y);
//	_tscanf(_T("%d, %d"), &pt.x, &pt.y);

	if (rect.PtInRect(pt)) {
		printf("내부\n");
//		_tprintf(_T("내부\n"));
	}
	else {
		printf("외부\n");
//		_tprintf(_T("외부\n"));
	}
}

void problem09(void) {
	CTime curTime = CTime::GetCurrentTime();
	CString str = curTime.Format(_T("%Y-%m-%d %H:%M:%S"));
	printf("%s\n", str);
//	_tprintf(_T("%s\n"), str);
}

void problem10(void) {
	CTime curTime = CTime::GetCurrentTime();
	CString str = curTime.FormatGmt(_T("%A, %B, %Y"));
	printf("%s\n", str);
//	_tprintf(_T("%s\n"), str);
}

void problem11(void) {
	CTime curTime = CTime::GetCurrentTime();
	CTimeSpan timeSpan(1000 - 1, 0, 0, 0);
	CTime futureTime = curTime + timeSpan;

	CString futStr = futureTime.Format(_T("%Y-%m-%d"));
	printf("오늘로 부터 1000일 후는 %s\n", futStr);
//	_tprintf(_T("오늘로 부터 1000일 후는 %s\n"), futStr);
}